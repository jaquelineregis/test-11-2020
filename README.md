# Test 11-2020

## Tecnologias:
- Python
- Selenium
- Selenium webdriver


## Configuração:

- Selenium:
    - https://selenium-python.readthedocs.io/installation.html

- geckodriver:
    - https://github.com/mozilla/geckodriver/releases/tag/v0.28.0
    - tar xvvf geckodriver-v0.28.0-linux64.tar.gz
    - sudo cp geckodriver /usr/local/bin/

## Runner

- python3 scraper.py

## Result
_(Ao iniciar será pedido que seja colocado o nome do filme)_

Digite o nome do filme: *plano b*

_(O resultado da raspagem será em json contendo as informações e os filmes)_

```json
{
  "infos": {
    "movie_searched": "plano b",
    "total_movies_finded": 2,
    "total_movies_scrapy": 2
  },
  "movies": [
    {
      "name": "PLANO B",
      "production_year": "2010",
      "type_film": "longa-metragem",
      "budget": "35 000 000 $",
      "idioms": "Ingl\u00eas",
      "scores": {
        "press": "-",
        "users": "-",
        "adorocinema": "-",
        "my_friends": "-"
      }
    },
    {
      "name": "PLANO B",
      "production_year": "2013",
      "type_film": "longa-metragem",
      "budget": "-",
      "idioms": "Portugu\u00eas",
      "scores": {
        "press": "-",
        "users": "-",
        "adorocinema": "-",
        "my_friends": "-"
      }
    }
  ]
}
```

## Descrição do campo do json

- *infos*
    - *movie_searched*: Campo referente ao nome do filme buscado
    - *total_movies_finded*: Quantidade de filmes com o nome semelhante que foram encontrados
    - *total_movies_scrapy*: Quantidade de filmes que conseguiu ser capturado pela ferramenta.
- *movies*
    - *name*: Nome do filme encontrado
    - *production_year*: Ano de produção
    - *type_film*: Tipo de filme
    - *budget*: Orçamento
    - *idioms*: Idiomas
    - *scores*
        - *press*: Pontuação referente a impresa
        - *users*: Pontuação referente aos usuários
        - *adorocinema*: Pontuação referente ao adoro cinema
        - *my_friends*: Pontuação referente aos meus amigos