import json
import math
import re

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


movie_searched = input("Digite o nome do filme: ")


browser = webdriver.Firefox()
browser.get(f"http://www.adorocinema.com/busca/movie/?q={movie_searched}")
browser.implicitly_wait(2)


def results(**kwargs):
    movies = kwargs.get("movies", [])
    return {
        "infos": {
            "movie_searched": movie_searched,
            "total_movies_finded": kwargs.get("total_movies_finded", 0),
            "total_movies_scrapy": len(movies),
        },
        "movies": movies,
    }


def total_movies_finded():
    try:
        qt_movies_finded = browser.find_element_by_xpath(
            "//section[contains(@class, 'section movies-results')]/div[2]"
        )
    except NoSuchElementException:
        return 0
    else:
        qt_list = re.findall("\d+", qt_movies_finded.text)
        return int(qt_list[0]) if qt_list else 0


def xpath_score(partial_href):
    try:
        browser.implicitly_wait(0)
        score = browser.find_element_by_xpath(
            f"//div[contains(@class, 'rating-holder')]//a[contains(@href, '{partial_href}')]/following::div//span[contains(@class, 'stareval-note')]"
        ).text
    except NoSuchElementException:
        return "-"
    else:
        return score


def get_info_movies(ttl_movies_finded):
    if ttl_movies_finded != 0:
        qt_movies_find_in_first_page = browser.find_elements_by_xpath(
            "//section[contains(@class, 'section movies-results')]//a[contains(@class, 'xXx meta-title-link')]"
        )
        pages = math.ceil(ttl_movies_finded / len(qt_movies_find_in_first_page))

        movies = []
        for number in range(1, pages + 1):
            browser.get(
                f"http://www.adorocinema.com/busca/movie/?q={movie_searched}&page={number}"
            )
            browser.implicitly_wait(2)

            movies_find_in_page = browser.find_elements_by_xpath(
                "//section[contains(@class, 'section movies-results')]//a[contains(@class, 'xXx meta-title-link')]"
            )
            for movie in movies_find_in_page:
                browser.execute_script(
                    f"window.open('{movie.get_attribute('href')}','_blank');"
                )
                browser.switch_to_window(browser.window_handles[1])
                browser.implicitly_wait(10)

                element = browser.find_element_by_xpath(
                    "//div[contains(@class, 'more-trigger js-more-trigger')]"
                )
                browser.implicitly_wait(30)

                browser.execute_script(
                    "arguments[0].setAttribute('class','more-trigger js-more-trigger active')",
                    element,
                )
                browser.implicitly_wait(10)

                xpath_more_hidden = lambda phrase: browser.find_element_by_xpath(
                    f"//div[contains(@class, 'more-hidden')]//span[contains(text(), '{phrase}')]/following::span"
                ).text
                movies.append(
                    {
                        "name": browser.find_element_by_xpath(
                            "//div[contains(@class, 'titlebar-title titlebar-title-lg')]"
                        ).text,
                        "production_year": xpath_more_hidden("Ano de produção"),
                        "type_film": xpath_more_hidden("Tipo de filme"),
                        "budget": xpath_more_hidden("Orçamento"),
                        "idioms": xpath_more_hidden("Idiomas"),
                        "scores": {
                            "press": xpath_score("/criticas/imprensa/"),
                            "users": xpath_score("/criticas/espectadores/"),
                            "adorocinema": xpath_score("/criticas-adorocinema/"),
                            "my_friends": "-",  # xpath_score("/criticas/.../"),
                        },
                    }
                )

                browser.close()
                browser.switch_to_window(browser.window_handles[0])

        browser.close()
        return movies

    else:
        browser.close()
        return []


def runner():
    ttl_movies_finded = total_movies_finded()
    movies = get_info_movies(ttl_movies_finded)
    return results(total_movies_finded=ttl_movies_finded, movies=movies)


print(json.dumps(runner(), indent=2, ensure_ascii=False))
